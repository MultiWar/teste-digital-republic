import { AVAILABLE_PAINT_BUCKET_SIZES, COVERAGE_BY_LITER_M2, DOOR_AREA_M2, DOOR_DIMENSIONS_M, WINDOW_AREA_M2 } from "./info"

export const isWallAreaWithinBounds = (wallArea: number) => {
    // returns true if wall area is between 1 and 15 m² 
    return (wallArea >= 1 && wallArea <= 15)
}

const isWallsProportionsCorrect = (totalWallArea: number, numberOfWindows: number, numberOfDoors: number) => {
    // returns true if area of all walls is at least double the area of all windows and doors
    return (totalWallArea / 2) >= ((numberOfWindows * WINDOW_AREA_M2) + (numberOfDoors * DOOR_AREA_M2))
}

export const isWallTallEnough = (wallHeight: number) => {
    // returns true if wall height is more than 30cm more than the door's height
    return (wallHeight - DOOR_DIMENSIONS_M[1]) >= 0.3
}

const findActualWallArea = (totalWallArea: number, numberOfWindows: number, numberOfDoors: number) => {
    // returns the wall area not covered by windows and/or doors
    return totalWallArea - ((numberOfWindows * WINDOW_AREA_M2) + (numberOfDoors * DOOR_AREA_M2))
}

const findLitersAmount = (area: number) => {
    // returns the rounded up amount of liters necessary to paint the walls
    if(area % COVERAGE_BY_LITER_M2 === 0) {
        return area / COVERAGE_BY_LITER_M2
    }

    if(Math.floor(area / COVERAGE_BY_LITER_M2) + 0.5 >= area / COVERAGE_BY_LITER_M2) {
        return area / COVERAGE_BY_LITER_M2
    }

    return Math.ceil(area / COVERAGE_BY_LITER_M2)
}

const resolvePhrasing = (area: number, amountOfBuckets: number[]) => {
    let phrase = `To paint ${area}m² of wall (windows and doors areas already discounted on calculation) you will need`
    for(let i = 0; i < amountOfBuckets.length; i++) {
        if(amountOfBuckets[i] !== 0) {
            phrase += ` ${amountOfBuckets[i]} buckets of ${AVAILABLE_PAINT_BUCKET_SIZES[i]}L,`
        }
    }

    return phrase
}

export const findListOfBucketsNecessary = (totalWallArea: number, numberOfWindows: number, numberOfDoors: number) => {
    // checks for possible errors and returns the result
    if(!isWallsProportionsCorrect(totalWallArea, numberOfWindows, numberOfDoors)) {
        throw new Error('The area of the windows and doors must be at most half of the areas of the walls')
    }

    const area = findActualWallArea(totalWallArea, numberOfWindows, numberOfDoors)
    let liters = findLitersAmount(area)
    let amountOfBuckets = [0, 0, 0, 0]

    AVAILABLE_PAINT_BUCKET_SIZES.reduce((acc, curr, index) => {
        if(acc >= curr) {
            amountOfBuckets[index] = Math.floor(acc / curr)
            if(index === 3) {
                amountOfBuckets[index] = Math.ceil(acc / curr)
            }

            liters -= curr * Math.floor(acc / curr)
            return acc - (curr * Math.floor(acc / curr))
        }
        return acc
    }, liters)

    return resolvePhrasing(totalWallArea, amountOfBuckets)
}