import React, { useState } from "react"
import { Button } from "../components/Form/Button"
import { WallController } from "../components/WallController"
import { findListOfBucketsNecessary, isWallAreaWithinBounds, isWallTallEnough } from "../utils/functions"
import '../styles/calculator.css'

type Wall = {
    width: string,
    height: string,
    numberOfDoors: string,
    numberOfWindows: string
}

export const Calculator = () => {
    const [firstWall, setFirstWall] = useState({
        width: "0",
        height: "0",
        numberOfDoors: "0",
        numberOfWindows: "0"
    })

    const [secondWall, setSecondWall] = useState({
        width: "0",
        height: "0",
        numberOfDoors: "0",
        numberOfWindows: "0"
    })

    const [thirdWall, setThirdWall] = useState({
        width: "0",
        height: "0",
        numberOfDoors: "0",
        numberOfWindows: "0"
    })

    const [fourthWall, setFourthWall] = useState({
        width: "0",
        height: "0",
        numberOfDoors: "0",
        numberOfWindows: "0"
    })

    const [result, setResult] = useState('')

    const checkWall = (wall: Wall, wallNumber: number) => {
        if(Number(wall.numberOfDoors) > 0) {
            if(!isWallTallEnough(Number(wall.height))) {
                throw new Error(`Wall ${wallNumber} isn't tall enough`)
            }
        }

        if(!isWallAreaWithinBounds(Number(wall.width) * Number(wall.height))) {
            throw new Error(`Wall ${wallNumber} must be bigger than 1m² and smaller than 15m²`)
        }
    }

    const OnSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault()

        const totalWallArea = 
            (Number(firstWall.width) * Number(firstWall.height)) +
            (Number(secondWall.width) * Number(secondWall.height)) +
            (Number(thirdWall.width) * Number(thirdWall.height)) +
            (Number(fourthWall.width) * Number(fourthWall.height))
        
        const totalNumberOfWindows = 
            Number(firstWall.numberOfWindows) + Number(secondWall.numberOfWindows) +
            Number(thirdWall.numberOfWindows) + Number(fourthWall.numberOfWindows)

        const totalNumberOfDoors = 
            Number(firstWall.numberOfDoors) + Number(secondWall.numberOfDoors) +
            Number(thirdWall.numberOfDoors) + Number(fourthWall.numberOfDoors)

        try {
            checkWall(firstWall, 1)
            checkWall(secondWall, 2)
            checkWall(thirdWall, 3)
            checkWall(fourthWall, 4)

            const string = findListOfBucketsNecessary(totalWallArea, totalNumberOfWindows, totalNumberOfDoors)

            setResult(string)
        } catch(e) {
            alert(e)
        }
    }

    return (
        <main>
            <form onSubmit={e => OnSubmit(e)}>
                <div>
                    <WallController wall={firstWall} setWall={setFirstWall} wallNumber={1} />
                    <WallController wall={secondWall} setWall={setSecondWall} wallNumber={2} />
                    <WallController wall={thirdWall} setWall={setThirdWall} wallNumber={3} />
                    <WallController wall={fourthWall} setWall={setFourthWall} wallNumber={4} />
                </div>

                <Button type='submit' text='Calculate' />
            </form>

            {result && <p>{result}</p>}
        </main>
    )
}