import React from 'react';
import { Calculator } from './pages/calculator';
import './styles/global.css'

function App() {
  return (
    <div className="App">
      <Calculator />
    </div>
  );
}

export default App;
