import { Input } from "../Form/Input"

interface WallControllerProps {
    wall: Wall
    setWall: (w: Wall) => void,
    wallNumber: number
}

type Wall = {
    width: string,
    height: string,
    numberOfDoors: string,
    numberOfWindows: string
}


export const WallController = ({wall, setWall, wallNumber}: WallControllerProps) => {
    const setWallWidth = (width: string) => {
        setWall({...wall, width: width})
    }
    const setWallHeight = (height: string) => {
        setWall({...wall, height: height})
    }
    const setNumberOfDoors = (NoD: string) => {
        setWall({...wall, numberOfDoors: NoD})
    }
    const setNumberOfWindows = (NoW: string) => {
        setWall({...wall, numberOfWindows: NoW})
    }


    return (
        <div className='wallControllerWrapper'>
            <h2>Wall {wallNumber}</h2>
            <Input label='width' name={`wall${wallNumber}-width`} type='number' value={wall.width} onChange={setWallWidth} />
            <Input label='height' name={`wall${wallNumber}-height`} type='number' value={wall.height} onChange={setWallHeight} />
            <Input label='number of doors' name={`wall${wallNumber}-doors`} type='number' value={wall.numberOfDoors} onChange={setNumberOfDoors} />
            <Input label="number of windows" name={`wall${wallNumber}-windows`} type="number" value={wall.numberOfWindows} onChange={setNumberOfWindows} />
        </div>
    )
}