import './style.css'

interface ButtonProps {
    type: "button" | "submit" | "reset",
    onClick?: () => void,
    text: string,
}

export const Button = ({ type, onClick, text }: ButtonProps) => {
    return (
        <button className="formButton" type={type} onClick={onClick}>{text}</button>
    )
}