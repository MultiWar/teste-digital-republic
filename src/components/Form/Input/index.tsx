import './style.css'

interface InputProps {
    name: string,
    label: string,
    type: "text" | "number",
    value: string,
    onChange: (v: string) => void
}

export const Input = ({name, label, type, value, onChange}: InputProps) => {
    return (
        <div className="inputDiv">
            <label htmlFor={name}>{label}</label>
            <input type={type} name={name} value={value} onChange={v => onChange(v.target.value)} />
        </div>
    )
}