import { findListOfBucketsNecessary, isWallAreaWithinBounds, isWallTallEnough } from "../utils/functions"

describe("isWallAreaWithinBounds function", () => {
    it('should return false if area < 1', () => {
        expect(isWallAreaWithinBounds(0.5)).toBe(false)
    })

    it('should return false if area > 15', () => {
        expect(isWallAreaWithinBounds(16)).toBe(false)
    })

    it('should return true if area >= 1 and area <= 15', () => {
        expect(isWallAreaWithinBounds(10)).toBe(true)
    })
})

describe('isWallTallEnough function', () => {
    it('should return false if wall height is < 2.2', () => {
        expect(isWallTallEnough(2)).toBe(false)
    })

    it('should return true if wall height is >= 2.2', () => {
        expect(isWallTallEnough(2.2)).toBe(true)
    })
})

describe('findListOfBucketsNecessary function', () => {
    it('should throw an error if the area of all walls is < double the area of all windows + doors', () => {
        try {
            findListOfBucketsNecessary(20, 3, 3)
        } catch(error: any) {
            expect(error?.message).toBe('The area of the windows and doors must be at most half of the areas of the walls')
        }
    })

    it('should return the correct amount and phrasing if the area of all walls is >= double the area of all windows + doors', () => {
        expect(findListOfBucketsNecessary(20, 1, 1)).toBe('To paint 20m² of wall (windows and doors areas already discounted on calculation) you will need 1 buckets of 2.5L, 2 buckets of 0.5L,')
    })
})